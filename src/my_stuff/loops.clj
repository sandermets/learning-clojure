(ns my-stuff.loops)

(def adjs ["normal"
           "too small"
           "too big"
           "is swimming"])

(defn alice-is [in out]
  (if (empty? in)
    out ;returns out
    (alice-is
      (rest in); leaveout the first element and pass the rest of the vector to function
      (conj out (str "Alice is " (first in))) ; conjoin: https://clojuredocs.org/clojure.core/conj
    )
  )
)
