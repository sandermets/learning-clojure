(ns my-stuff.conditions)

(defn simple [x y] 
  (* x y)
)

(defn notsimple [x y] 
  (let [a (* x y)] a))

(defn ifsample []
  (if (> (notsimple 2 2) 50)
    "greater than 50"
    "not greater"
  )
)
