# my-stuff

Learning Clojure

## Installation

    lein run
    lein test
    lein repl

## Usage

    $ java -jar my-stuff-0.1.0-standalone.jar [args]

## License

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
